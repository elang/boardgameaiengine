autopep8==1.2.1
coverage==4.0
pep8==1.6.2
wsgiref==0.1.2
mock==1.3.0
pymongo==3.1
psutil==4.3.1
